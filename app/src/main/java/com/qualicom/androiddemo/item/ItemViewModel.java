package com.qualicom.androiddemo.item;

import com.qualicom.androiddemo.vo.ItemVo;

public class ItemViewModel {
    private ItemVo itemVo;
    private String name;
    private String description;

    public ItemViewModel(ItemVo itemVo) {
        this.itemVo = itemVo;
        this.name = itemVo.getName();
        this.description = itemVo.getDescription();
    }

    public ItemVo getItemVo() {
        return itemVo;
    }

    public void setItemVo(ItemVo itemVo) {
        this.itemVo = itemVo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
