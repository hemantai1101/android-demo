package com.qualicom.androiddemo.item;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.qualicom.androiddemo.App;
import com.qualicom.androiddemo.R;
import com.qualicom.androiddemo.databinding.FragmentItemBinding;
import com.qualicom.androiddemo.vo.ItemVo;
import com.qualicom.androiddemo.web.ItemRestClient;

public class ItemFragment extends Fragment {
    private FragmentItemBinding binding;
    private ItemVo itemVo;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item, container, false);
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        binding.progressBar.setVisibility(View.VISIBLE);

        if (getArguments() != null) {
            Integer itemId = getArguments().getInt("itemId");
            new ItemRestClient().get(itemId, new ItemRestClient.OnGetResponseListener<ItemVo>() {
                @Override
                public void response(ItemVo item) {
                    itemVo = item;
                    binding.tvName.setText(itemVo.getName());
                    binding.tvDescription.setText(itemVo.getDescription());
                    binding.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void error(String errorMessage) {
                    binding.progressBar.setVisibility(View.GONE);
                    Toast.makeText(App.getContext(), "Failed to get item", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
