package com.qualicom.androiddemo.item;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.qualicom.androiddemo.R;
import com.qualicom.androiddemo.databinding.ViewItemBinding;
import com.qualicom.androiddemo.item.ItemViewHolder;
import com.qualicom.androiddemo.item.ItemViewModel;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemViewHolder> {
    private List<ItemViewModel> viewModelList = new ArrayList<>();
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(ItemViewModel item);
    }

    public ItemAdapter() {
        if (viewModelList != null)
            this.viewModelList.addAll(viewModelList);
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ViewItemBinding binding = DataBindingUtil.inflate(layoutInflater, i, viewGroup, false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder viewHolder, int i) {
        viewHolder.bind(viewModelList.get(i), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return viewModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.view_item;
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public List<ItemViewModel> getViewModelList() {
        return viewModelList;
    }

    public void setViewModelList(List<ItemViewModel> viewModelList) {
        this.viewModelList = viewModelList;
    }
}
