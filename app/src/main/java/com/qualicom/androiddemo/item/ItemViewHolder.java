package com.qualicom.androiddemo.item;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.qualicom.androiddemo.databinding.ViewItemBinding;

public class ItemViewHolder extends RecyclerView.ViewHolder {
    private ViewItemBinding binding;

    public ItemViewHolder(ViewItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(final ItemViewModel vm, final ItemAdapter.OnItemClickListener listener) {
        binding.tvName.setText(vm.getName());

        binding
            .getRoot()
            .setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(vm);
                }
            });
    }
}
