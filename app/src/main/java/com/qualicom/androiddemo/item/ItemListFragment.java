package com.qualicom.androiddemo.item;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.qualicom.androiddemo.App;
import com.qualicom.androiddemo.R;
import com.qualicom.androiddemo.databinding.FragmentItemListBinding;
import com.qualicom.androiddemo.vo.ItemVo;
import com.qualicom.androiddemo.web.ItemRestClient;

import java.util.ArrayList;
import java.util.List;

public class ItemListFragment extends Fragment {
    private FragmentItemListBinding binding;
    private List<ItemVo> itemVoList = new ArrayList<>();
    private ItemAdapter itemAdapter = new ItemAdapter();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        binding.progressBar.setVisibility(View.VISIBLE);

        new ItemRestClient().list(new ItemRestClient.OnListResponseListener() {
            @Override
            public void response(List<ItemVo> items) {
                itemVoList = items;

                List<ItemViewModel> itemViewModelList = new ArrayList<>();
                for (ItemVo itemVo : itemVoList)
                    itemViewModelList.add(new ItemViewModel(itemVo));

                itemAdapter.setViewModelList(itemViewModelList);
                itemAdapter.notifyDataSetChanged();
                binding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void error(String errorMessage) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(App.getContext(), "Failed to get item list", Toast.LENGTH_SHORT).show();
            }
        });

        LinearLayoutManager lmItemList = new LinearLayoutManager(getContext());
        lmItemList.setOrientation(LinearLayoutManager.VERTICAL);

        itemAdapter.setOnItemClickListener(new ItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ItemViewModel item) {
                Intent intent = new Intent(getActivity(), ItemActivity.class);
                intent.putExtra("itemId", item.getItemVo().getId());
                startActivity(intent);
            }
        });

        binding
            .rvItemList
            .setLayoutManager(lmItemList);

        binding
            .rvItemList
            .setAdapter(itemAdapter);
    }
}
