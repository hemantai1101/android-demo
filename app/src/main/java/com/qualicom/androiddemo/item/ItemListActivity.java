package com.qualicom.androiddemo.item;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.qualicom.androiddemo.R;
import com.qualicom.androiddemo.databinding.ActivityItemListBinding;

public class ItemListActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityItemListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_item_list);

        Log.i(getClass().getName(), "AppLinkTesting");

        Intent appLinkIntent = getIntent();
        Uri appLinkData = appLinkIntent.getData();
        if (appLinkData != null) {
            String itemId = appLinkData.getQueryParameter("itemId");
            if (itemId != null) {
                Intent intent = new Intent(this, ItemActivity.class);
                intent.putExtra("itemId", Integer.parseInt(itemId));
                startActivity(intent);
            }
        }
    }
}
