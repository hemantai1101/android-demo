package com.qualicom.androiddemo.item;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.qualicom.androiddemo.R;
import com.qualicom.androiddemo.databinding.ActivityItemBinding;

public class ItemActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityItemBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_item);

        Fragment itemFragment = getSupportFragmentManager().findFragmentById(R.id.fragmentItem);

        if (itemFragment != null)
            itemFragment.setArguments(getIntent().getExtras());
    }
}
