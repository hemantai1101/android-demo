package com.qualicom.androiddemo;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class App extends Application {
    private static App instance;
    private static RequestQueue requestQueue;
    private static ImageLoader imageLoader;

    public static Context getContext(){
        return instance;
    }

    public static String getServerUrl() {
        return BuildConfig.SERVER_URL;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();

        requestQueue = Volley.newRequestQueue(instance.getApplicationContext());

        imageLoader = new ImageLoader(requestQueue,
            new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap>
                    cache = new LruCache<String, Bitmap>(20);

                @Override
                public Bitmap getBitmap(String url) {
                    return cache.get(url);
                }

                @Override
                public void putBitmap(String url, Bitmap bitmap) {
                    cache.put(url, bitmap);
                }
            });
    }

    public static synchronized RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public static ImageLoader getImageLoader() {
        return imageLoader;
    }
}
