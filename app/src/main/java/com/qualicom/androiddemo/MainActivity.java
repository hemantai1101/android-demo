package com.qualicom.androiddemo;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.qualicom.androiddemo.databinding.ActivityMainBinding;
import com.qualicom.androiddemo.item.ItemListActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // App Initialization
        Intent intent = new Intent(this, ItemListActivity.class);
        startActivity(intent);
    }
}
