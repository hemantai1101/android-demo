package com.qualicom.androiddemo.web;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qualicom.androiddemo.App;
import com.qualicom.androiddemo.vo.ItemVo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

public class ItemRestClient {
    private OnListResponseListener onListResponseListener;
    private OnGetResponseListener onGetResponseListener;
    private OnCreateResponseListener onCreateResponseListener;

    public ItemRestClient() {

    }

    public void get(Integer id, OnGetResponseListener listener) {
        this.onGetResponseListener = listener;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
            String.format("%s/%d", buildResourceUrl(App.getServerUrl(), getResourceName()), id),
            null,
            new Response.Listener<JSONObject>() {
                @Override
                @SuppressWarnings(value = "unchecked")
                public void onResponse(JSONObject response) {
                    Log.d(getClass().getSimpleName(), response.toString());
                    Gson gson = new Gson();
                    ItemVo itemVo = gson.fromJson(response.toString(), ItemVo.class);
                    onGetResponseListener.response(itemVo);
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(getClass().getName(), error.toString());
                    onGetResponseListener.error("Unknown Error");
                }
            });

        App.getRequestQueue().add(request);
    }

    public void create(ItemVo vo, OnCreateResponseListener listener) {

    }

    public void list(OnListResponseListener listener) {
        this.onListResponseListener = listener;

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
            buildResourceUrl(App.getServerUrl(), getResourceName()),
            null,
            new Response.Listener<JSONArray>() {
                @Override
                @SuppressWarnings(value = "unchecked")
                public void onResponse(JSONArray response) {
                    Log.d(getClass().getSimpleName(), response.toString());
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<ItemVo>>() {}.getType();
                    List<ItemVo> r = gson.fromJson(response.toString(), listType);
                    onListResponseListener.response(r);
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(getClass().getName(), error.toString());
                    onListResponseListener.error("Unknown Error");
                }
            });

        App.getRequestQueue().add(request);
    }

    private String buildResourceUrl(String serverUrl, String resource) {
        return String.format("%s/%s", serverUrl, resource);
    }

    private String getResourceName() {
        return "items";
    }

    public interface OnListResponseListener {
        void response(List<ItemVo> item);

        void error(String errorMessage);
    }

    public interface OnGetResponseListener<T> {
        void response(T item);

        void error(String errorMessage);
    }

    public interface OnCreateResponseListener<T> {
        void response();

        void error(String errorMessage);
    }
}
